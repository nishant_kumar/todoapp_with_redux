import React from 'react';
import Todo from './Todo';
import { connect } from 'react-redux';
import { deleteTodo } from '../redux/actions/todoActionCreator'

class TodoList extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            todos: []
        }
    }

    deleteTodo (id) {
        this.props.deleteTodo(id);
        this.setState({ todos: this.props.todos });
    }

    render () {
        let todos = this.props.todos;
        return (
            <div className="todo-list">
                { todos ? todos.map((item, i) => {
                    return <Todo key={ i } { ...item } deleteTodo={ () => this.deleteTodo(i) }></Todo>
                }) : null
                }
            </div>
        )
    }
}

function mapStateToProps (state) {
    return { todos: state.todos };
}

export default connect(mapStateToProps, { deleteTodo })(TodoList)

import React from 'react';
import { connect } from 'react-redux';
import { TOGGLE_TODO } from '../redux/actions/constants'

class Todo extends React.Component {
    toggleTodo () {
        this.props.dispatch({
            type: TOGGLE_TODO,
            id: this.props.id
        })
    }

    render () {
        const { completed, text } = this.props;
        return (
            <div className="list">
                <div className={ completed ? 'todo-item selected' : 'todo-item' }>
                    <span onClick={ () => this.toggleTodo() }>{ completed ? "👌" : "👋" }</span>
                    <span onClick={ () => this.toggleTodo() }>{ text }</span>
                </div>
                <span className="close" onClick={ () => this.props.deleteTodo(this.props.id) }>X</span>
            </div>
        )
    }
}

export default connect()(Todo)

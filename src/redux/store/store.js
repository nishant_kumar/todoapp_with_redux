import { createStore } from 'redux';
import reducer from '../reducers/'

const initialState = {
    todos: []
}

let store = createStore(reducer, initialState)

export default store;

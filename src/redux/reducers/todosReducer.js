import { ADD_TODO, DELETE_TODO, TOGGLE_TODO } from '../actions/constants';

function todos (state = [], action) {
    switch (action.type) {
        case ADD_TODO:
            return [...state, action.payload]

        case TOGGLE_TODO:
            let _todos = state.map((item) => {
                if (item.id === action.id) {
                    item.completed = !item.completed
                }
                return item;
            })
            return _todos

        case DELETE_TODO:
            state.splice(action.id, 1);
            return state;

        default:
            return state;
    }
}

export default todos;

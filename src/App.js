import React from 'react';
import TodoList from './components/TodoList'
import AddTodo from "./components/AddTodo";

function App () {
    return (
        <div className="todo-app">
            <div className="title">I am Todo app with Redux</div>
            <AddTodo></AddTodo>
            <TodoList></TodoList>
        </div>
    )
}

export default App;
